# SystemPermissions #

To protect the system's integrity and the user's privacy, Android runs each app in a limited access sandbox. If the app wants to use resources or information outside of its sandbox, the app has to explicitly request permission. Depending on the type of permission the app requests, the system may grant the permission automatically, or the system may ask the user to grant the permission.

You declare that your app needs a permission by listing the permission in the App Manifest.

Depending on how sensitive the permission is, the system might grant the permission automatically, or the device user might have to grant the request. For example, if your app requests permission to turn on the device's flashlight, the system grants that permission automatically. But if your app needs to read the user's contacts, the system asks the user to approve that permission.

Depending on the platform version, the user grants the permission either when they install the app (on Android 5.1 and lower) or while running the app (on Android 6.0 and higher).

*CODE*

//check permissions

int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

//in permission is not granted ask user

if (permission != PackageManager.PERMISSION_GRANTED) {
// We don't have permission so prompt the user
ActivityCompat.requestPermissions(this, PERMISSIONS_VIDEO_CHOOSER, 1);
} 
else 
{
//do what ever you want because your app has permission
}